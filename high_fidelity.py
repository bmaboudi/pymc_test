import numpy as np
from dolfin import *
from mshr import *

set_log_level(50)

def output_indices(x):
	return (x[0]*x[0] + x[1]*x[1] <  2) and (x[0]*x[0] + x[1]*x[1] >  1 - 3e-3)

class random_field_circle(UserExpression):
	def set_params(self,model_params):
		self.rf = model_params['rf']
		self.num_rf = self.rf.size
		self.y1 = np.array([ 0 ])
		self.y2 = np.array([ 0 ])
		self.h1 = 3.0
		self.h2 = 1.0
		self.scale = 0.5
	def eval(self, values, x):
		if( x[1] > 0 ):
			values[0] = self.h2
			for i in range(self.num_rf):
				k = i+1
				values[0] += self.scale*self.rf[i]*np.sin(k*x[0])*np.sin(k*x[1])/k
		else:
			values[0] = self.h1
			for i in range(self.num_rf):
				k = i+1
				values[0] += self.scale*self.rf[i]*np.sin(k*x[0])*np.sin(k*x[1])/k

class source_term(UserExpression):
	def eval(self, values, x):
		values[0] = 50*exp(-(pow(x[0] , 2) + pow(x[1], 2)) / 0.1)

def boundary(x):
	return x[0] < -1 + DOLFIN_EPS or x[0] > 1.0 - DOLFIN_EPS

class high_fidelity():
	def __init__(self, num_out = 100, num_modes=1):
		#domain = Rectangle(Point(-1,-1), Point(1,1))
		#self.mesh = generate_mesh(domain, 40)
		#mesh_file = File('mesh.xml')
		#mesh_file << self.mesh
		self.mesh = Mesh('mesh.xml')
		self.V = FunctionSpace(self.mesh, 'CG', 1)

		self.u0 = Constant(0.0)
		self.bc = DirichletBC(self.V, self.u0, boundary)

		self.u = TrialFunction(self.V)
		self.v = TestFunction(self.V)
		
		FEM_el = self.V.ufl_element()
		self.f = source_term(element = FEM_el)

		self.alpha = random_field_circle(element = FEM_el)
		self.num_out = num_out
		self.num_modes = num_modes

	def set_params(self, model_params):
		self.alpha.set_params(model_params)

	def assemble_rhs(self):
		self.L = self.f*self.v*dx
		#b = assemble(self.L)

	def assemble_bilinear_matrix(self):
		self.a = self.alpha*self.u.dx(0)*self.v.dx(0)*dx + self.alpha*self.u.dx(1)*self.v.dx(1)*dx

	def solve(self):
		self.sol = Function(self.V)
		solve(self.a == self.L, self.sol, bcs = self.bc)

		x = np.linspace(-1+1e-3,1-1e-3, int(self.num_out/2) )
		out_vec = []
		for i in range( len(x) ):
			out_vec.append( self.sol(x[i], 1-1e-3) )
		for i in range( len(x) ):
			out_vec.append( self.sol(x[i], -1+1e-3) )

		out_vec = np.reshape(out_vec,[-1])
		return out_vec

	def save_solution(self):
		file = File("solution.pvd")
		file << self.sol

	def save_random_field(self):
		alpha_func = interpolate(self.alpha, self.V)
		file = File('random_field.pvd')
		file << alpha_func

def find_lines(model_params):
	y1 = np.random.uniform(-1,1,1)
	y2 = np.random.uniform(-1,1,1)

	model_params['y1'] = y1
	model_params['y2'] = y2
	return model_params

if __name__=='__main__':
	hf = high_fidelity(num_out=200)

	model_params = {}
	model_params['rf'] = np.random.uniform(0,1,10)

	hf.set_params(model_params)
	hf.assemble_rhs()
	hf.assemble_bilinear_matrix()
	hf.solve()
	hf.save_solution()
	hf.save_random_field()