import numpy as np
import pymc3 as pm
import nn_network as neural
import torch
import theano
import theano.tensor as tt
import high_fidelity as hf

def gradients(vals, func, releps=1e-3, abseps=None, mineps=1e-9, reltol=1e-3, epsscale=0.5):

	grads = np.zeros(len(vals))

	# maximum number of times the gradient can change sign
	flipflopmax = 10.

	# set steps
	if abseps is None:
		if isinstance(releps, float):
			eps = np.abs(vals)*releps
			eps[eps == 0.] = releps  # if any values are zero set eps to releps
			teps = releps*np.ones(len(vals))
		elif isinstance(releps, (list, np.ndarray)):
			if len(releps) != len(vals):
				raise ValueError("Problem with input relative step sizes")
			eps = np.multiply(np.abs(vals), releps)
			eps[eps == 0.] = np.array(releps)[eps == 0.]
			teps = releps
		else:
			raise RuntimeError("Relative step sizes are not a recognised type!")
	else:
		if isinstance(abseps, float):
			eps = abseps*np.ones(len(vals))
		elif isinstance(abseps, (list, np.ndarray)):
			if len(abseps) != len(vals):
				raise ValueError("Problem with input absolute step sizes")
			eps = np.array(abseps)
		else:
			raise RuntimeError("Absolute step sizes are not a recognised type!")
		teps = eps

	# for each value in vals calculate the gradient
	count = 0
	for i in range(len(vals)):
		# initial parameter diffs
		leps = eps[i]
		cureps = teps[i]

		flipflop = 0

		# get central finite difference
		fvals = np.copy(vals)
		bvals = np.copy(vals)

		# central difference
		fvals[i] += 0.5*leps  # change forwards distance to half eps
		bvals[i] -= 0.5*leps  # change backwards distance to half eps
		cdiff = (func(fvals)-func(bvals))/leps

#		print(fvals)
#		print(bvals)
#		input()

		while 1:
			fvals[i] -= 0.5*leps  # remove old step
			bvals[i] += 0.5*leps

			# change the difference by a factor of two
			cureps *= epsscale
#			if cureps < mineps or flipflop > flipflopmax:
#				# if no convergence set flat derivative (TODO: check if there is a better thing to do instead)
#				warnings.warn("Derivative calculation did not converge: setting flat derivative.")
#				grads[count] = 0.
#				break
			leps *= epsscale

			# central difference
			fvals[i] += 0.5*leps  # change forwards distance to heps
			bvals[i] -= 0.5*leps  # change backwards distance to half eps
			cdiffnew = (func(fvals)-func(bvals))/leps

			if cdiffnew == cdiff:
				grads[count] = cdiff
				break

			# check whether previous diff and current diff are the same within reltol
			rat = (cdiff/cdiffnew)
			if np.isfinite(rat) and rat > 0.:
				# gradient has not changed sign
				if np.abs(1.-rat) < reltol:
					grads[count] = cdiffnew
					break
				else:
					cdiff = cdiffnew
					continue
			else:
				cdiff = cdiffnew
				flipflop += 1
				continue

			count += 1

		return grads

class nn_model():
	def __init__(self, path):
		eval_model = neural.network_eval('./model.pt')
		self.eval_func = eval_model.eval_model

	def eval(self,x):
		torch_x = torch.from_numpy( np.reshape(x,[1,-1]) ).float()
		return self.eval_func(torch_x)

	def loglike(self,x,y_obs,sigma):
		torch_x = torch.from_numpy( np.reshape(x,[1,-1]) ).float()
		y = self.eval_func( torch_x )
		return -(0.5/sigma**2)*np.sum((y-y_obs)**2)

class LogLike(tt.Op):
	itypes = [tt.dvector]
	otypes = [tt.dscalar]
	def __init__(self, loglike_func, y_obs, sigma):
		self.likelihood = loglike_func
		self.y_obs = y_obs
		self.sigma=sigma

	def perform(self, node, inputs, outputs):
		x, = inputs
		logl = self.likelihood( x, self.y_obs, self.sigma )
		outputs[0][0] = np.array(logl)

class LogLikeWithGrad(tt.Op):
	itypes = [tt.dvector]
	otypes = [tt.dscalar]
	def __init__(self, loglike_func, y_obs, sigma):
		self.likelihood = loglike_func
		self.y_obs = y_obs
		self.sigma=sigma

		self.logpgrad = LogLikeGrad(self.likelihood, self.y_obs, self.sigma)

	def perform(self, node, inputs, outputs):
		x, = inputs
		logl = self.likelihood( x, self.y_obs, self.sigma )
		outputs[0][0] = np.array(logl)

	def grad(self, inputs, g):
		x, = inputs
		return [g[0]*self.logpgrad(x)]

class LogLikeGrad(tt.Op):
	itypes = [tt.dvector]
	otypes = [tt.dvector]

	def __init__(self, loglike_func, y_obs, sigma):
		self.likelihood = loglike_func
		self.y_obs = y_obs
		self.sigma=sigma

	def perform(self, node, inputs, outputs):
		x, = inputs

		def lnlike(values):
			return self.likelihood(values, self.y_obs, self.sigma)

		grads = gradients(x, lnlike)
		outputs[0][0] = grads

cov = np.eye(10)
mu = np.zeros(10)

mapping = nn_model('./model.pt')

high_fid = hf.high_fidelity(num_out=200)
model_params = {}
model_params['rf'] = np.random.uniform(0,1,10)
high_fid.set_params(model_params)
high_fid.assemble_rhs()
high_fid.assemble_bilinear_matrix()
y_obs = high_fid.solve()

sigma = np.sqrt(0.05)
#logl = LogLike(mapping.loglike, y_obs, sigma)
logl = LogLikeWithGrad(mapping.loglike, y_obs, sigma)

with pm.Model() as model:
	x = pm.MvNormal('x', mu=mu, cov=cov, shape=(10))
	#sigma = pm.HalfCauchy('sigma', beta=10, testval=1.)

	#pm.DensityDist( 'likelihood', mapping.loglike, observed={'x':x, 'y_obs':y_obs, 'sigma':sigma} )
	pm.DensityDist('likelihood', lambda v: logl(v), observed={'v': x})
	trace = pm.sample(200, tune=100, discard_tuned_samples=True, chains=2)
	#mean_field = pm.fit(method='advi')



data = trace['x']
shape = data.shape
print(np.sum(data,axis=0)/shape[0])
print(model_params['rf'])